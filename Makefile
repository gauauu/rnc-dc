
#rncdc.elf -> [dcbuild bin example.elf] -> main.bin -> [dcbuild iso] -> mygame.iso -> [dcbuild iso] -> mygame.iso.cdi

LEVELS_C	 = $(patsubst levels/%.lvl, bin/gen/%.c, $(wildcard levels/*.lvl))
MKDIRS   = @mkdir -p bin/obj && mkdir -p bin/gen


rncdc.iso.cdi: mygame.iso
	tools/dcbuild cdi
	mv -f mygame.iso.cdi rncdc.iso.cdi

mygame.iso: main.bin
	tools/dcbuild iso

main.bin: rncdc.elf
	tools/dcbuild bin rncdc.elf

rncdc.elf: FORCE bin/gen/levelPointers.c $(LEVELS_C)
	tools/dcbuild make -f Makefile.kos


bin/gen/levelPointers.c:  $(wildcard levels/*.lvl) tools/generateLevelPointers.py
	$(MKDIRS)
	tools/generateLevelPointers.py

#convert level text format to binary source
bin/gen/%.c : levels/%.lvl
	$(MKDIRS)
	tools/level2Data.py $<

idea: CMakeLists.txt
	echo "ok"

CMakeLists.txt: $(wildcard $(SRCDIR)/*.*)
	tools/generateCMake.py

FORCE: ;

.PHONY: clean run

clean:
	rm -rf bin
	rm -rf iso
	rm -f IP.BIN IP.TMPL ip.txt main.bin *.iso *.iso.cdi *.elf .*.swp

run: rncdc.iso.cdi
	~/kos-2.0.0-src/redream rncdc.iso.cdi


