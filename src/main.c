#include "common.h"
#include "sprite.h"
#include "mc.h"
#include "items.h"
#include "ctrl.h"
#include "bg.h"
#include "level.h"
#include "sfx.h"
#include "dead.h"
#include "obstacle.h"
#include "audio.h"
#include "text.h"
#include "title.h"
#include <png/png.h>
#include <string.h>
#include <kos/thread.h>

/* font data */
extern char wfont[];

/* textures */
pvr_ptr_t font_tex;
pvr_ptr_t back_tex;
pvr_ptr_t back_tex2;
pvr_ptr_t load_bg_tex;
pvr_ptr_t title_tex;

//pvr_ptr_t mc_tex[10];
pvr_ptr_t mc_sheet;
pvr_ptr_t mc_title_sheet;
pvr_ptr_t obstacle_sheet;


char *data;

char debugBuffer[256];

bool restart = false;
bool skipDraw = false;
bool main_paused;
bool stubTexturesLoaded = false;
bool texturesLoaded = false;


void stub_textures_init() {
	text_init();
	load_bg_tex = pvr_mem_malloc(32 * 32 * 2);
	png_to_texture("/rd/load_bg.png", load_bg_tex, PNG_NO_ALPHA);

}

void *textures_init(void *param) {


	back_tex = pvr_mem_malloc(256 * 256 * 2);
	png_to_texture("/rd/bg0-1.png", back_tex, PNG_NO_ALPHA);
	back_tex2 = pvr_mem_malloc(256 * 256 * 2);
	png_to_texture("/rd/bg0-2.png", back_tex2, PNG_NO_ALPHA);

	title_tex = pvr_mem_malloc(256 * 256 * 2);
	png_to_texture("/rd/title_bg.png", title_tex, PNG_NO_ALPHA);

	mc_sheet = sprite_loadTexture("/rd/mc.png", 512, 512);
	mc_title_sheet = sprite_loadTexture("/rd/mc_title.png", 512, 512);
	obstacle_sheet = sprite_loadTexture("/rd/spikes.png", 512, 512);
	texturesLoaded = true;
}


/* base y coordinate */
int y = 0;

/* draw one frame */
void draw_frame(void) {
	pvr_wait_ready();
	pvr_scene_begin();

	pvr_list_begin(PVR_LIST_OP_POLY);

	bg_renderOpaque(back_tex, back_tex2);
	pvr_list_finish();

	pvr_list_begin(PVR_LIST_TR_POLY);

	if (main_paused) {
		text_drawTextCentered("Paused", 215);
	}
	bg_renderAlpha(obstacle_sheet);



	if (mc_status != MC_STATUS_DEAD) {
		mc_render(mc_sheet);
	} else {
		dead_render(mc_sheet);
	}

	obstacle_renderAll(obstacle_sheet);
	items_render();

	pvr_list_finish();
	pvr_scene_finish();

}

/* romdisk */
extern uint8 romdisk_boot[];
KOS_INIT_ROMDISK(romdisk_boot);


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

void snd_update();

int main(void) {

//	dbgio_dev_select("fb");
//	dbgio_enable();


	pvr_init_defaults();


	//load the textures we need for the loading screen
	stub_textures_init();

	texturesLoaded = false;
	thd_create(1, textures_init, NULL);

//	textures_init();
	while (!texturesLoaded) {
		pvr_set_bg_color(1.0f, 1.0f, 1.0f);
		pvr_wait_ready();
		pvr_scene_begin();

		pvr_list_begin(PVR_LIST_OP_POLY);
		sprite_scaleToBackground(32, load_bg_tex);
		pvr_list_finish();

		pvr_list_begin(PVR_LIST_TR_POLY);
		text_drawTextCentered("Loading...", 200);
		pvr_list_finish();

		pvr_scene_finish();
	}


	audio_init();


	restart_game:

	music_playSong(MUSIC_MAIN_THEME);
	title_run();

	restart = false;
	mc_init();
	bg_init();
	items_init();
	level_init();
	obstacle_init();

	main_paused = false;


	level_showLevelScreen();

	while (1) {

		snd_update();

		skipDraw = false;
		text_debugLine = 1;

		if (level_complete) {
			level_showLevelScreen();
			if (restart == 1) {
				goto restart_game;
			}
			continue;
		}

		ctrl_update();

		if (ctrl_new & PAD_START) {
			main_paused = !main_paused;
			sfx_playSound(SFX_POWERUP);
		}


		if (main_paused) {
//			text_drawTextCentered("Paused", 10);
		} else {
			if (mc_status != MC_STATUS_DEAD) {
				bg_update();
				mc_update();
				items_update();
				obstacle_updateAll();
			} else {
				dead_update();
			}
			timer++;
		}

		if (!skipDraw) {
			draw_frame();
		}

		if (restart == 1) {
			goto restart_game;
		}
	}

}



#pragma clang diagnostic pop


































//
//
//
//
//
///* init font */
//void font_init() {
//	int i, x, y, c;
//	unsigned short *temp_tex;
//
//	font_tex = pvr_mem_malloc(256 * 256 * 2);
//	temp_tex = (unsigned short *) malloc(256 * 128 * 2);
//
//	c = 0;
//
//	for (y = 0; y < 128; y += 16)
//		for (x = 0; x < 256; x += 8) {
//			for (i = 0; i < 16; i++) {
//				temp_tex[x + (y + i) * 256 + 0] = 0xffff * ((wfont[c + i] & 0x80) >> 7);
//				temp_tex[x + (y + i) * 256 + 1] = 0xffff * ((wfont[c + i] & 0x40) >> 6);
//				temp_tex[x + (y + i) * 256 + 2] = 0xffff * ((wfont[c + i] & 0x20) >> 5);
//				temp_tex[x + (y + i) * 256 + 3] = 0xffff * ((wfont[c + i] & 0x10) >> 4);
//				temp_tex[x + (y + i) * 256 + 4] = 0xffff * ((wfont[c + i] & 0x08) >> 3);
//				temp_tex[x + (y + i) * 256 + 5] = 0xffff * ((wfont[c + i] & 0x04) >> 2);
//				temp_tex[x + (y + i) * 256 + 6] = 0xffff * ((wfont[c + i] & 0x02) >> 1);
//				temp_tex[x + (y + i) * 256 + 7] = 0xffff * (wfont[c + i] & 0x01);
//			}
//
//			c += 16;
//		}
//
//	pvr_txr_load_ex(temp_tex, font_tex, 256, 256, PVR_TXRLOAD_16BPP);
//}
//
////void text_init() {
////	int length = zlib_getlength("/rd/text.gz");
////	gzFile f;
////
////	data = (char *) malloc(length + 1); // I am not currently freeing it
////
////	f = gzopen("/rd/text.gz", "r");
////	gzread(f, data, length);
////	data[length] = 0;
////	gzclose(f);
////
////	printf("length [%d]\n", length);
////}
//
///* draw background */
//void draw_back(void) {
//	pvr_poly_cxt_t cxt;
//	pvr_poly_hdr_t hdr;
//	pvr_vertex_t vert;
//
//	pvr_poly_cxt_txr(&cxt, PVR_LIST_OP_POLY, PVR_TXRFMT_RGB565, 512, 512, back_tex, PVR_FILTER_BILINEAR);
//	pvr_poly_compile(&hdr, &cxt);
//	pvr_prim(&hdr, sizeof(hdr));
//
//	vert.argb = PVR_PACK_COLOR(1.0f, 1.0f, 1.0f, 1.0f);
//	vert.oargb = 0;
//	vert.flags = PVR_CMD_VERTEX;
//
//	vert.x = 1;
//	vert.y = 1;
//	vert.z = 1;
//	vert.u = 0.0;
//	vert.v = 0.0;
//	pvr_prim(&vert, sizeof(vert));
//
//	vert.x = 640;
//	vert.y = 1;
//	vert.z = 1;
//	vert.u = 1.0;
//	vert.v = 0.0;
//	pvr_prim(&vert, sizeof(vert));
//
//	vert.x = 1;
//	vert.y = 480;
//	vert.z = 1;
//	vert.u = 0.0;
//	vert.v = 1.0;
//	pvr_prim(&vert, sizeof(vert));
//
//	vert.x = 640;
//	vert.y = 480;
//	vert.z = 1;
//	vert.u = 1.0;
//	vert.v = 1.0;
//	vert.flags = PVR_CMD_VERTEX_EOL;
//	pvr_prim(&vert, sizeof(vert));
//}
//
//
///* draw one character */
//void draw_char(float x1, float y1, float z1, float a, float r, float g, float b, int c, float xs, float ys) {
//	pvr_vertex_t vert;
//	int ix, iy;
//	float u1, v1, u2, v2;
//
//	ix = (c % 32) * 8;
//	iy = (c / 32) * 16;
//	u1 = (ix + 0.5f) * 1.0f / 256.0f;
//	v1 = (iy + 0.5f) * 1.0f / 256.0f;
//	u2 = (ix + 7.5f) * 1.0f / 256.0f;
//	v2 = (iy + 15.5f) * 1.0f / 256.0f;
//
//	vert.flags = PVR_CMD_VERTEX;
//	vert.x = x1;
//	vert.y = y1 + 16.0f * ys;
//	vert.z = z1;
//	vert.u = u1;
//	vert.v = v2;
//	vert.argb = PVR_PACK_COLOR(a, r, g, b);
//	vert.oargb = 0;
//	pvr_prim(&vert, sizeof(vert));
//
//	vert.x = x1;
//	vert.y = y1;
//	vert.u = u1;
//	vert.v = v1;
//	pvr_prim(&vert, sizeof(vert));
//
//	vert.x = x1 + 8.0f * xs;
//	vert.y = y1 + 16.0f * ys;
//	vert.u = u2;
//	vert.v = v2;
//	pvr_prim(&vert, sizeof(vert));
//
//	vert.flags = PVR_CMD_VERTEX_EOL;
//	vert.x = x1 + 8.0f * xs;
//	vert.y = y1;
//	vert.u = u2;
//	vert.v = v1;
//	pvr_prim(&vert, sizeof(vert));
//}
//
///* draw a string */
//void draw_string(float x, float y, float z, float a, float r, float g, float b, char *str, float xs, float ys) {
//	pvr_poly_cxt_t cxt;
//	pvr_poly_hdr_t hdr;
//	float orig_x = x;
//
//	pvr_poly_cxt_txr(&cxt, PVR_LIST_TR_POLY, PVR_TXRFMT_ARGB4444, 256, 256, font_tex, PVR_FILTER_BILINEAR);
//	pvr_poly_compile(&hdr, &cxt);
//	pvr_prim(&hdr, sizeof(hdr));
//
//	while (*str) {
//		if (*str == '\n') {
//			x = orig_x;
//			y += 40;
//			str++;
//			continue;
//		}
//
//		draw_char(x, y, z, a, r, g, b, *str++, xs, ys);
//		x += 8 * xs;
//	}
//}


