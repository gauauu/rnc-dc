#include "obstacle.h"
#include "bg.h"
#include "level.h"
#include "items.h"
#include "mc.h"
#include "ctrl.h"
#include "sprite.h"
#include "text.h"
#include "audio.h"

u8 obstacle_x[OBSTACLE_COUNT];
u8 obstacle_y[OBSTACLE_COUNT];
u8 obstacle_type[OBSTACLE_COUNT];
u8 obstacle_state[OBSTACLE_COUNT];
u8 obstacle_screen[OBSTACLE_COUNT];
u8 obstacle_next;

u8 obstacle_framesSinceSpawn;

u8 obstacle_evenOdd;

u8 obstacle_thisRow;

int obstacle_animFrame = 0;
int obstacle_animTimer = 0;

//from NES sprite module
u16 sprite_sprite0Y;
s16 sprite_sprite0Dir;


bool obs_playedZap = false;

extern u8 temple_nt;

#define OB_LEFT 51
#define OB_RIGHT 172

#define OB_TILE_LSPIKE 0x17
#define OB_TILE_RSPIKE 0x15
#define OB_TILE_MINE   0x1D

#define OB_TILE_LASER_LEFT    0x1B
#define OB_TILE_LASER_RIGHT   0x19
#define OB_TILE_LASERS        0x1A

#define NEW_OBSTACLE_DEPRECATED(XPOS, TYPE) obstacle_y[obstacle_next] = bg_offscreenRow * 8; \
                                 obstacle_x[obstacle_next] = (XPOS); \
                                 obstacle_type[obstacle_next] = OBSTACLE_TYPE_##TYPE; \
                                 ++obstacle_next; \
                                 bg_renderQueue[temp3] = OB_TILE_##TYPE; \
                                 ++temp3; \
                                 bg_renderQueue[temp3] = OB_TILE_##TYPE + 1; \
                                 ++temp3; \
                                 obstacle_checkMax();

#define NEW_OBSTACLE_DOUBLED(XPOS, TYPE) temp = (XPOS); \
                                 ci = OBSTACLE_TYPE_##TYPE; \
                                 cj = OB_TILE_##TYPE; \
                                 tempC = OB_TILE_##TYPE; \
                                 ob__newObstacle();

#define NEW_OBSTACLE_TALL(XPOS, TYPE) temp = (XPOS); \
                                 ci = OBSTACLE_TYPE_##TYPE; \
                                 cj = OB_TILE_##TYPE; \
                                 tempC = OB_TILE_##TYPE + 16; \
                                 ob__newObstacle();


#define OB_LASERS_ACTIVE  (timer & B01000000)

#define OB_ANIM_DELAY 15

void obstacle_init(void) {
	for (ci = 0; ci <= OBSTACLE_MAX; ++ci) {
		obstacle_type[ci] = OBSTACLE_TYPE_UNUSED;
	}
	obstacle_framesSinceSpawn = 0;
	obstacle_next = 0;

	sprite_sprite0Y = (200 << 8);
	sprite_sprite0Dir = -100;
}

void obstacle_updateOne() {
	//y is cj
	cj = obstacle_y[ci];


	//figure out if it's new, or to be deleted
	if (cj == bg_offscreenRow) {
		//if it's at the offscreen row and no longer new, it's time to be deleted
		if (!(obstacle_state[ci] & OBSTACLE_FLAG_NEW)) {
			//I don't like this, moving it to where it gets rendered, which is bad, but easy
//			obstacle_state[ci] = OBSTACLE_FLAG_NONE;
//			obstacle_type[ci] = OBSTACLE_TYPE_UNUSED;
			return;
		}
	} else {
		//if it's not at the offscreen row, mark it as no longer new
		obstacle_state[ci] &= ~OBSTACLE_FLAG_NEW;
	}

	if (obstacle_type[ci] == OBSTACLE_TYPE_LASERS) {
		if ((timer == B01000000) || (timer == B11000000)){
			if (!obs_playedZap) {
				obs_playedZap = true;
				sfx_playZap();
			}
		}
	}

	//compare to player Y for collision
	if (mc_boundaryInverted) {
		tempB = (cj > mc_blockYMin || cj < mc_blockYMax);
	} else {
		tempB = (cj > mc_blockYMin && cj < mc_blockYMax);
	}

	if (tempB) {

		if (obstacle_type[ci] == OBSTACLE_TYPE_LASERS) {
			if (OB_LASERS_ACTIVE) {
				mc_dead();
			}
			return;
		}

		temp = obstacle_x[ci];
		//check exact point for collisions
		if ((temp > mc_collideLeft) &&
		    temp < mc_collideRight) {

			mc_dead();

		}
	}
}


//x is temp
//type is ci
//tile is cj
//2nd row tile is tempC
void ob__newObstacle() {
	obstacle_y[obstacle_next] = bg_offscreenRow;
	obstacle_x[obstacle_next] = temp * 16 + OB_LEFT;
	if (temp == 0) {
		obstacle_x[obstacle_next] -= 5;
	}
	if (temp == 7) {
		obstacle_x[obstacle_next] += 5;
	}
	obstacle_type[obstacle_next] = ci;
	obstacle_state[obstacle_next] = OBSTACLE_FLAG_NEW;
	obstacle_screen[obstacle_next] = camera_screen;
	++obstacle_next;

	tempD = ci;


	if (obstacle_next > OBSTACLE_MAX) {
		obstacle_next = 0;
	}
}

void obstacle_spawnNew(void) {
	//now start creating obstables
	level_getNextRow();

	obstacle_thisRow = temp;

	if (obstacle_thisRow == 0xFF) {
		//spawn item
		items_spawnForLevel();
	} else if (obstacle_thisRow == B01111110) {
		//fire shooter
		NEW_OBSTACLE_TALL(0, LASERS)

	} else {
		if (obstacle_thisRow & B10000000) {
			NEW_OBSTACLE_DOUBLED(0, LSPIKE)
		}
		if (obstacle_thisRow & B00000001) {
			NEW_OBSTACLE_DOUBLED(9, RSPIKE)
		}
		if (obstacle_thisRow & B01000000) {
			NEW_OBSTACLE_TALL(2, MINE)
		}
		if (obstacle_thisRow & B00100000) {
			NEW_OBSTACLE_TALL(3, MINE)
		}
		if (obstacle_thisRow & B00010000) {
			NEW_OBSTACLE_TALL(4, MINE)
		}
		if (obstacle_thisRow & B00001000) {
			NEW_OBSTACLE_TALL(5, MINE)
		}
		if (obstacle_thisRow & B00000100) {
			NEW_OBSTACLE_TALL(6, MINE)
		}
		if (obstacle_thisRow & B00000010) {
			NEW_OBSTACLE_TALL(7, MINE)
		}
	}

	++bg_renderIndex;
	obstacle_framesSinceSpawn = 0;

}

u8 camera_lastMsbY;


void obstacle_updateAll(void) {
	obs_playedZap = false;
	++obstacle_framesSinceSpawn;

	//see if we need to spawn anything
	temp = camera_lastMsbY ^ MSB(camera_y);
	if (temp & B00010000) {
		//TODO: make obstacle array bigger, force wrapping
		//instead of overflow
		obstacle_spawnNew();
		//spawn new obstable
	} else {
		//alternately, update everything
		//(I don't think we need to update
		//every frame do we?)
		for (ci = 0; ci < OBSTACLE_COUNT; ci++) {
			if (obstacle_type[ci] != OBSTACLE_TYPE_UNUSED) {
				obstacle_updateOne();
			}
		}
	}

	camera_lastMsbY = MSB(camera_y);
}


void obstacle_renderBottomSpikes(pvr_ptr_t sheet) {
	int i;
	int spriteYRender = 0;
	//update rendering animation
	if (MSB(sprite_sprite0Y) > 200) {
		sprite_sprite0Dir = -100;
	}
	if (MSB(sprite_sprite0Y) < 190) {
		sprite_sprite0Dir = 100;
	}
	sprite_sprite0Y += sprite_sprite0Dir;

	spriteYRender = MSB(sprite_sprite0Y);
	spriteYRender = DCRES(spriteYRender);


	for (i = 0; i < 608; i += 128) {
		sprite_renderFromSheetAtSize(i, spriteYRender - 25, false, sheet, 8, 128);
	}

}

void obstacle_renderAll(pvr_ptr_t obs_sheet) {

	int type;
	int screen;
	int renderY;
	int cel;
	bool hFlip = false;

	if (obstacle_animTimer >= OB_ANIM_DELAY) {
		obstacle_animFrame++;
		obstacle_animTimer = 0;
		if (obstacle_animFrame == 7) {
			obstacle_animFrame = 0;
		}
	}

	obstacle_animTimer++;


	for (tempJ = 0; tempJ < OBSTACLE_COUNT; ++tempJ) {
		type = obstacle_type[tempJ];
		if (type == OBSTACLE_TYPE_LSPIKE ||
		    type == OBSTACLE_TYPE_RSPIKE ||
		    type == OBSTACLE_TYPE_MINE
						) {


			if (type == OBSTACLE_TYPE_RSPIKE) {
				cel = 9;
				hFlip = true;
			} else if (type == OBSTACLE_TYPE_LSPIKE) {
				cel = 9;
				hFlip = false;
			} else {
				cel = obstacle_animFrame;
				hFlip = false;
			}

			screen = obstacle_screen[tempJ];

			//calculate Y (using method from NES laser rendering, modified for 16-bit render)
			renderY = obstacle_y[tempJ];
			renderY = renderY * 8 + 8;
			if (renderY == 240) {
				renderY = 0;
			}
			tempU16 = renderY;
			renderY += ((s16) camera_screen - (s16) screen) * 240;
			renderY = renderY * 2;
			renderY = renderY - SCREEN(camera_y);

			if (renderY > 460) {
				obstacle_state[tempJ] = OBSTACLE_FLAG_NONE;
				obstacle_type[tempJ] = OBSTACLE_TYPE_UNUSED;
			}

			sprite_renderFromSheetAtSize(DCXOFFSET(DCRES(obstacle_x[tempJ]) + 20), renderY - 18, hFlip, obs_sheet, cel, 32);

		}

		//always render the outer laser thingies
		if (type == OBSTACLE_TYPE_LASERS) {
			//calculate Y (using method from NES laser rendering, modified for 16-bit render)
			screen = obstacle_screen[tempJ];
			renderY = obstacle_y[tempJ];
			renderY = renderY * 8 + 8;
			if (renderY == 240) {
				renderY = 0;
			}
			tempU16 = renderY;
			renderY += ((s16) camera_screen - (s16) screen) * 240;
			renderY = renderY * 2;
			renderY = renderY - SCREEN(camera_y);

			sprite_renderFromSheetAtSize(135, renderY - 18, false, obs_sheet, 10, 32);
			sprite_renderFromSheetAtSize(475, renderY - 18, true, obs_sheet, 10, 32);

			if (OB_LASERS_ACTIVE) {
				int offset = (7 * timer) % 512;
				int width = 305;
				int needsWraparound = 0;
				if (offset > (512 - 305)) {
					width = 512 - offset;
					needsWraparound = 1;
				}

				sprite_renderElec(167, renderY - 18, width, offset, obs_sheet);
				if (needsWraparound) {
					sprite_renderElec(167 + width, renderY - 18, 305 - width, 0, obs_sheet);
				}

			}

			if (renderY > 460) {
				obstacle_state[tempJ] = OBSTACLE_FLAG_NONE;
				obstacle_type[tempJ] = OBSTACLE_TYPE_UNUSED;
			}

		}

	}


	obstacle_renderBottomSpikes(obs_sheet);
}






