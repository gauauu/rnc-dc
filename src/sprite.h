

#ifndef RNC_DC_DRAW_H
#define RNC_DC_DRAW_H

#include "common.h"


pvr_ptr_t sprite_loadTexture(char * name, int w, int h);
void sprite_render(int screen_x, int screen_y, int w, int h, bool hFlip, pvr_ptr_t texture);

//assumes 4x4 grid of 128x128 sprites in a 512 sheet texture
void sprite_renderFromSheet(int screen_x, int screen_y, bool hFlip, pvr_ptr_t texture, int cel) ;
void sprite_renderFromSheetAtSize(int screen_x, int screen_y, bool hFlip, pvr_ptr_t texture, int cel, int size);

void sprite_renderBackdrop(int offset, pvr_ptr_t back_tex);
void sprite_scaleToBackground(int textureSize, pvr_ptr_t tex);

void sprite_renderElec(int screen_x, int screen_y, int w, int xStart, pvr_ptr_t texture);

#endif //RNC_DC_DRAW_H
