
#ifndef RNC_DC_OBSTACLE_H
#define RNC_DC_OBSTACLE_H

#include "common.h"

#define OBSTACLE_COUNT 32
#define OBSTACLE_MAX (OBSTACLE_COUNT - 1)

#define OBSTACLE_TYPE_UNUSED 0
#define OBSTACLE_TYPE_LSPIKE 1
#define OBSTACLE_TYPE_RSPIKE 2
#define OBSTACLE_TYPE_MINE   3
#define OBSTACLE_TYPE_LASERS 4

#define OBSTACLE_FLAG_NONE   0
#define OBSTACLE_FLAG_NEW    1
#define OBSTACLE_FLAG_DEL    2


void obstacle_init(void);
void obstacle_updateAll(void);
void obstacle_renderAll(pvr_ptr_t sheet);


#endif //RNC_DC_OBSTACLE_H
