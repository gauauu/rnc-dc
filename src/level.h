
#ifndef RNC_DC_LEVEL_H
#define RNC_DC_LEVEL_H


#include "common.h"

extern u8 level_current;
extern u8 level_complete;
extern u8 level_index;
extern u8 level_max;

void level_restartLevel(void);
void level_init(void);
void level_getNextRow(void);
void level_setLevel(int level);

void level_showLevelScreen(void);

#endif //RNC_DC_LEVEL_H
