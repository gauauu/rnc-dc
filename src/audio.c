
#include "audio.h"
#include <oggvorbis/sndoggvorbis.h>

int currentlyPlaying;

sfxhnd_t sfx_powerup;
sfxhnd_t sfx_zap;
sfxhnd_t sfx_explode;

void audio_init() {
	//snd_stream_init();
	snd_init();
	sndoggvorbis_init();
	currentlyPlaying = -99;


	sfx_powerup = snd_sfx_load("/rd/audio/powerup.wav");
	sfx_zap = snd_sfx_load("/rd/audio/electricity.wav");
	sfx_explode = snd_sfx_load("/rd/audio/explode.wav");
}

void snd_update() {
}

char * songPaths[] = {
				"/rd/audio/pulse2.ogg",
				"/rd/audio/cave.ogg",
				"/rd/audio/positive.ogg",
				"/rd/audio/AlexBerozaArtNow.ogg",
				"/rd/audio/pulse2.ogg",
};

void music_playSong(int songId) {

	if (songId == currentlyPlaying) {
		return;
	}
	if (currentlyPlaying != -99) {
		sndoggvorbis_stop();
	}

	currentlyPlaying = songId;

	char * songPath = songPaths[songId];
	sndoggvorbis_start(songPath, 1);
}



void sfx_playPowerup() {
	snd_sfx_play(sfx_powerup, 254, 128);
}

void sfx_playZap() {
	snd_sfx_play(sfx_zap, 254, 128);
}

void sfx_playExplode() {
	snd_sfx_play(sfx_explode, 254, 128);
}
