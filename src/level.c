#include <png/png.h>
#include "level.h"
#include "items.h"
#include "ctrl.h"
#include "common.h"
#include "sprite.h"
#include "mc.h"
#include "bg.h"
#include "dead.h"
#include "level.h"
#include "title.h"
#include "obstacle.h"
#include "text.h"
#include "audio.h"
#include <png/png.h>

#define LEVEL_SCREEN_TIMER (350)
#define LEVEL_SCREEN_ALLOW_SKIP (LEVEL_SCREEN_TIMER - 60)

#define LEVEL_START 0
#define LEVEL_VICTORY 5


u8 level_current;
u8 level_index;

u8 level_complete;
u16 level_screenTimer;

extern u8 level0_max;
extern u8 level0test_max;
extern u8 level0;
extern u8 level0test;

u8 *level_rowPtr;

u8 level_upcoming;

extern u8 firstTime;

extern u8 *level_maps[];

extern u8 *level_maxes[];

extern u8 *level_text1Pointers[];

extern u8 *level_text2Pointers[];

extern u8 *level_inst0Pointers[];

extern u8 *level_inst1Pointers[];

u8 level_max;


void level_restartLevel(void) {
	level_rowPtr = level_maps[level_current];
	tempPtr = level_maxes[level_current];
	level_index = *tempPtr;
	level_max = level_index;
}

void level_init(void) {
	level_current = LEVEL_START;
	level_complete = true;
	level_restartLevel();
}


void level_setComplete() {
	level_complete = true;
	++level_current;
	level_restartLevel();
}

void level_getNextRow(void) {
	temp = level_rowPtr[level_index];
#ifdef CHEAT_B_SKIP
	if (ctrl_current & PAD_B) {
		level_index = 0;
	}
#endif
	if (level_index == 0) {
		level_setComplete();
	} else {
		--level_index;
	}
	level_upcoming = temp;
}





extern u8 text_level0;
extern u8 text_level0title;
extern u8 text_level0inst0;
extern u8 text_level0inst1;

void level_screenSetup(void);


void level_showRoboNinja(void) {
//	ci = 108;
//	cj = 88;
//
//	temp = title_animFrame;
//	if (temp == 4) {
//		temp = 2;
//	}
//	if (temp == 5) {
//		temp = 1;
//	}
//	temp = temp * 4;
//
//	temp2 = 0;
//	tempA = 4;
//	tempB = 5;
//	sprite_drawBlock();
}


extern u8 text_level0inst2;
extern u8 text_level0inst3;

bool level_newTextureLoaded = false;

char * level_bgs1[] = {
				"/rd/bg-cave-1.png",
				"/rd/tunnel0.png",
				"/rd/city0.png",
				"/rd/universe0.png",
				"/rd/bg0-1.png",
};

char * level_bgs2[] = {
				"/rd/bg-cave-2.png",
				"/rd/tunnel1.png",
				"/rd/city1.png",
				"/rd/universe1.png",
				"/rd/bg0-2.png",
};

int level_songs[] = {
				MUSIC_MAIN_THEME,
				MUSIC_CAVE,
				MUSIC_POSITIVE,
				MUSIC_ART,
				MUSIC_MAIN_THEME,
};

void * level_loadNewLevelTexture(void * param) {

	png_to_texture(level_bgs1[level_current], back_tex, PNG_NO_ALPHA);
	png_to_texture(level_bgs2[level_current], back_tex2, PNG_NO_ALPHA);
	level_newTextureLoaded = true;

	return NULL;
}


char * level_texts[] = {
				"Level 0",
				"Training",
				"A to jump.  Down to slide.",
				"You can steer mid-air.",
				"Hold back while jumping",
				"to scale upwards.",

				"Level 1",
				"A little harder",
				"A great start.",
				"Keep up the good work!",
				"",
				"",

				"Level 2",
				"Double-Jump",
				"Collect the Double-Jump",
				"to jump twice in air",
				"",
				"",

				"Level 3",
				"Climber",
				"Once found, hold up to",
				"slowly scale a wall",
				"",
				"",

				"Level 4",
				"Final Round",
				"Almost there!",
				"Stay on target!",
				"",
				"",

};

void level_showLevelScreen(void) {

	int text_base = level_current * 6;

	if (level_current == LEVEL_VICTORY) {
		victory_run();
		return;
	}

	switch (level_current) {
		case 0:
			bg_tileModifier = 0;
			break;
		case 1:
			bg_tileModifier = 1;
			break;
		default:
			bg_tileModifier = 0;
			break;
	}

	level_newTextureLoaded = false;
	thd_create(1, level_loadNewLevelTexture, NULL);

	level_complete = false;
	level_screenTimer = LEVEL_SCREEN_TIMER;

	title_animFrame = 0;
	title_animTimer = TITLE_ANIM_TIME;


	while (true) {
		title_allLoopStuff();

		pvr_wait_ready();
		pvr_scene_begin();
		pvr_list_begin(PVR_LIST_OP_POLY);

		if (!level_newTextureLoaded) {
			sprite_scaleToBackground(32, load_bg_tex);
			pvr_list_finish();
			pvr_scene_finish();
			continue;
		}


		sprite_scaleToBackground(32, load_bg_tex);
//		sprite_renderBackdrop(0, back_tex);
		pvr_list_finish();
		music_playSong(level_current);

		pvr_list_begin(PVR_LIST_TR_POLY);
		text_drawTextCentered(level_texts[text_base], 115);
		text_drawTextCentered(level_texts[text_base + 1], 145);
		title_showRoboNinja();
		text_drawTextSmall(level_texts[text_base + 2], 200, 320);
		text_drawTextSmall(level_texts[text_base + 3], 200, 340);
		text_drawTextSmall(level_texts[text_base + 4], 200, 360);
		text_drawTextSmall(level_texts[text_base + 5], 200, 380);

		pvr_list_finish();
		pvr_scene_finish();


		if (level_current > 0) {
			--level_screenTimer;
		}

		if (level_screenTimer == 0) {
			break;
		}

		if (ctrl_new & PAD_B) {
			break;
		}

		if (ctrl_new & PAD_START) {
			break;
		}

		if ((ctrl_current & PAD_A) && (level_screenTimer < LEVEL_SCREEN_ALLOW_SKIP)) {
			break;
		}


		--title_animTimer;
		if (title_animTimer == 0) {
			title_animTimer = TITLE_ANIM_TIME;
			++title_animFrame;
			if (title_animFrame == 6) {
				title_animFrame = 0;
			}
		}
	}

	//prep to go back to main game
	mc_init();
	bg_init();
	items_init();
	obstacle_init();
	skipDraw = true;

}
