
#ifndef RNC_DC_TEXT_H
#define RNC_DC_TEXT_H

#include "common.h"

void text_init();
void text_test();

void text_drawTextCentered(const char * str, int y);
void text_drawTextCenteredBig(const char * str, int y);
void text_drawText(const char * str, int x, int y);
void text_drawTextSmall(const char * str, int x, int y);

void text_debug(int a);
extern int text_debugLine;

#define DRAW_TEXT_CENTERED(str, nesPosY) text_drawTextCentered(str, DCRES(nesPosY * 8))
#define DRAW_TEXT(str, nesPosX, nesPosY) text_drawText(str, DCRES(nesPosX * 8), DCRES(nesPosY * 8))

#endif //RNC_DC_TEXT_H
