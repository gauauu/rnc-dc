#include "bg.h"
#include "ctrl.h"
#include "sprite.h"
#include "level.h"
#include "text.h"


u16 camera_y;
u16 camera_vy;
u16 camera_screen;


u8 bg_offscreenRow;
u16 bg_offscreenRowAddress;
u16 bg_offscreenAttrAddress;

u8 bg_renderQueue[128];
u8 bg_renderIndex;


u8 bg_attrRowToRender;
u8 bg_attrMirror[64];

u8 bg_tileModifier;

#define BG_ATTR_SKIP 255


void bg_calcOffscreenRowAddress(void) {
	//first calc screen row number
	//that's offscreen
	bg_offscreenRow = MSB(camera_y) - 16;
	if (bg_offscreenRow > 240) {
		bg_offscreenRow -= 16;
	}
	bg_offscreenRow /= 8;
//	bg_offscreenRow += 1;

	//translate that to an address
	bg_offscreenRowAddress = 0x2000 + bg_offscreenRow * 32;
}

void bg_update(void) {
	camera_y -= camera_vy;
	if (MSB(camera_y) > 240) {
		camera_y -= (16 << 8);
		camera_screen++;
	}
	bg_calcOffscreenRowAddress();
	bg_renderIndex = 0;
	bg_attrRowToRender = BG_ATTR_SKIP;

#ifdef CHEAT_SEL_PAUSE
	if (ctrl_current & PAD_SELECT) {
		camera_vy = 0;
	}
#endif
}

void bg_renderOpaque(pvr_ptr_t tex, pvr_ptr_t tex2) {
	float num = camera_screen * 480 + (480 - SCREEN(camera_y));
	float den = level_max * 32 + 750;
	float percentDone =  num /  den;
	float renderAt = 480.0 * percentDone;
//	sprintf(debugBuffer, "%.2f / %.2f = %.2f * 480 = %.2f", num, den, percentDone, renderAt);
//sprintf(debugBuffer, "%d %d",  camera_screen, SCREEN(camera_y));

	sprite_renderBackdrop(-479 + renderAt, tex);
	sprite_renderBackdrop(renderAt , tex2);


//	int camera_renderY = 480 - SCREEN(camera_y);
//	if ((camera_screen % 2) == 0) {
//		sprite_renderBackdrop(camera_renderY, tex);
//		sprite_renderBackdrop(camera_renderY - 480, tex2);
//	} else {
//		sprite_renderBackdrop(camera_renderY, tex2);
//		sprite_renderBackdrop(camera_renderY - 480, tex);
//	}

}

void bg_renderAlpha(pvr_ptr_t obs_sheet) {
	int camera_renderY = 480 - SCREEN(camera_y);

	int i;

	int top = 11;
	int bottom = 15;
	if ((camera_screen % 2) == 1) {
		top = 15;
		bottom = 11;
	}

	top = 11;
	bottom = 11;

	for (i = -480; i < 480; i += 120) {
		sprite_renderFromSheetAtSize(117, i + camera_renderY, false, obs_sheet, top, 60);
		sprite_renderFromSheetAtSize(117, i + camera_renderY + 60, false, obs_sheet, bottom, 60);

		sprite_renderFromSheetAtSize(469, i + camera_renderY, false, obs_sheet, top, 60);
		sprite_renderFromSheetAtSize(469, i + camera_renderY + 60, false, obs_sheet, bottom, 60);
	}

	text_drawText(debugBuffer, 100, 100);


}

void bg_init(void) {
	camera_y = 0;
	camera_vy = 150;
#ifdef CHEAT_NOSCROLL
	camera_vy = 0;
#endif
	camera_screen = 0;

	bg_renderIndex = 0;
	bg_attrRowToRender = BG_ATTR_SKIP;
}
