
#ifndef RNC_DC_TITLE_H
#define RNC_DC_TITLE_H

#include "common.h"

void title_run(void);

void credits_run(void);

void victory_run(void);

//let other things reuse this
void title_allLoopStuff(void);
void title_tearDown(void);
void title_showRoboNinja(void);

extern u8 title_animTimer;
extern u8 title_animFrame;

#define TITLE_CREDIT_TIME (60 * 8)
#define TITLE_ANIM_TIME 15

#endif //RNC_DC_TITLE_H
