
#ifndef RNC_DC_CTRL_H
#define RNC_DC_CTRL_H


#include "common.h"

extern u8 ctrl_current;
extern u8 ctrl_new;

void ctrl_update(void);
void ctrl_debug(void);


#endif //RNC_DC_CTRL_H
