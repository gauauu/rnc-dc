
#include "text.h"

#include <plx/font.h>
#include <plx/sprite.h>
#include <plx/list.h>
#include <plx/dr.h>

plx_font_t * fnt;
plx_fcxt_t * cxt;
plx_fcxt_t * cxt_small;
plx_fcxt_t * cxt_big;

void text_init() {
	fnt = plx_font_load("/rd/axaxax.txf");
	cxt = plx_fcxt_create(fnt, PVR_LIST_TR_POLY);
	cxt->size = 40;

	cxt_small = plx_fcxt_create(fnt, PVR_LIST_TR_POLY);
	cxt_small->size = 19;

	cxt_big = plx_fcxt_create(fnt, PVR_LIST_TR_POLY);
	cxt_big->size = 60;

}


void text_test() {
	point_t w;
	w.x = 30.0f;
	w.y = 50.0f;
	w.z = 10.0f;


	plx_fcxt_begin(cxt);
	plx_fcxt_setpos_pnt(cxt, &w);
	plx_fcxt_draw(cxt, "This is a test!");
	plx_spr_inp(256, 256, 320, 240, 20, 0xffffffff);    // texture test
	plx_fcxt_end(cxt);
}


void text_drawTextCentered(const char * str, int y) {
	//void plx_fcxt_str_metrics(plx_fcxt_t * cxt, const char * str,
	//	float * outleft, float * outup, float * outright, float *outdown);

	float left;
	float up;
	float right;
	float down;

	plx_fcxt_str_metrics(cxt, str, &left, &up, &right, &down);
	text_drawText(str, 640 / 2 - (right / 2), y);

}

int text_debugLine;
char text_debugBuffer[100];
void text_debug(int a) {
	sprintf(text_debugBuffer, "%d (%02X)", a, a);
	text_drawTextSmall(text_debugBuffer, 40, 40 * text_debugLine + 40);
	text_debugLine++;
}

void text_drawTextCenteredBig(const char * str, int y) {
	//void plx_fcxt_str_metrics(plx_fcxt_t * cxt, const char * str,
	//	float * outleft, float * outup, float * outright, float *outdown);

	float left;
	float up;
	float right;
	float down;
	point_t w;

	plx_fcxt_str_metrics(cxt_big, str, &left, &up, &right, &down);
	w.x = 640 / 2 - (right / 2);
	w.y = y;
	w.z = 10.0f;


	plx_fcxt_begin(cxt_big);
	plx_fcxt_setpos_pnt(cxt_big, &w);
	plx_fcxt_draw(cxt_big, str);
	plx_fcxt_end(cxt_big);
}

void text_drawText(const char * str, int x, int y) {
	point_t w;
	w.x = x;
	w.y = y;
	w.z = 10.0f;


	plx_fcxt_begin(cxt);
	plx_fcxt_setpos_pnt(cxt, &w);
	plx_fcxt_draw(cxt, str);
	plx_fcxt_end(cxt);

}

void text_drawTextSmall(const char * str, int x, int y) {
	point_t w;
	w.x = x;
	w.y = y;
	w.z = 10.0f;


	plx_fcxt_begin(cxt_small);
	plx_fcxt_setpos_pnt(cxt_small, &w);
	plx_fcxt_draw(cxt_small, str);
	plx_fcxt_end(cxt_small);
}
