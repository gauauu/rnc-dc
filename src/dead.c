#include "dead.h"
#include "sprite.h"
#include "sprite.h"
#include "mc.h"
#include "bg.h"
#include "ctrl.h"
#include "items.h"
#include "level.h"
#include "obstacle.h"
#include "title.h"
#include "text.h"

#define DEAD_VELOCITY  0x01
#define DEAD_TIME      60

#define DEAD_GO_CONTINUE 0
#define DEAD_GO_END      1

#define DEAD_ANIM_TIME 7

//#define RENDER_PART(x,y,tile) ci = MSB(dead_##x); \
//                              cj = MSB(dead_##y); \
//                              temp = (tile); \
//                              sprite_draw();\


extern u8 text_dontgiveup;
extern u8 text_tryagain;
extern u8 text_deaths2;
extern u8 text_continue;
extern u8 text_end;


char dead_deathBuffer[100];


u8 dead_timer;

u16 dead_sourceY;
u16 dead_sourceX;

u16 dead_minusX;
u16 dead_plusX;
u16 dead_minusY;
u16 dead_plusY;

u8 dead_gameOverSelected;

extern void start(void);

void dead_begin(void) {
	dead_sourceY = mc_renderY + 30;
	dead_sourceX =  MSB(mc_x) * 2 + (LSB(mc_x) >> 7) + 58;

	dead_minusX = dead_sourceX;
	dead_plusX = dead_sourceX;

	dead_minusY = dead_sourceY;
	dead_plusY = dead_sourceY;
	dead_timer = 0;
}


void dead_renderGameOverSelector(void) {
	cj = 22 * 8 - 2;
	if (dead_gameOverSelected == DEAD_GO_CONTINUE) {
		cj = 20 * 8 - 2;
	}
	ci = 10 * 8;
	sprite_renderFromSheetAtSize(DCRES(ci) - 16, DCRES(cj) - 28, false, mc_sheet, 11, 32);
}



void dead_loadDeathTextBuffer(void) {
	sprintf(dead_deathBuffer, "Deaths: %d", mc_deaths);
}


void dead_drawRoboNinja(void) {

	int cel;

	if (title_animFrame < 4) {
		cel = title_animFrame + 12;
	} else {
		cel = (8 - title_animFrame) + 11;
	}

	sprite_renderFromSheet(DCRES(120), DCRES(80), false, mc_sheet, cel);
}

void dead_gameOverScreen(void) {

	title_animFrame = 0;
	title_animTimer = DEAD_ANIM_TIME;
	dead_loadDeathTextBuffer();

	dead_gameOverSelected = DEAD_GO_CONTINUE;

	while (true) {
		title_allLoopStuff();

		pvr_wait_ready();
		pvr_scene_begin();

		pvr_list_begin(PVR_LIST_OP_POLY);
		sprite_scaleToBackground(32, load_bg_tex);
//		sprite_renderBackdrop(0, back_tex);
		pvr_list_finish();

		pvr_list_begin(PVR_LIST_TR_POLY);


	  DRAW_TEXT_CENTERED("Keep at it!", 4);
	  DRAW_TEXT_CENTERED("Try again?", 6);
	  DRAW_TEXT_CENTERED(dead_deathBuffer, 9);

		DRAW_TEXT("Continue", 13, 20);
		DRAW_TEXT("End", 13, 22);


		dead_renderGameOverSelector();
		dead_drawRoboNinja();

		pvr_list_finish();
		pvr_scene_finish();

		if (ctrl_current & PAD_DOWN) {
			dead_gameOverSelected = DEAD_GO_END;
		}

		if (ctrl_current & PAD_UP) {
			dead_gameOverSelected = DEAD_GO_CONTINUE;
		}

		if (ctrl_new & PAD_SELECT) {
			dead_gameOverSelected = 1 - (dead_gameOverSelected);
		}

		if (ctrl_new & PAD_START) {
			break;
		}

		if (ctrl_new & PAD_A) {
			break;
		}


		--title_animTimer;
		if (title_animTimer == 0) {
			title_animTimer = DEAD_ANIM_TIME * 2;
			++title_animFrame;
			if (title_animFrame == 7) {
				title_animFrame = 0;
			}
		}

	}

	if (dead_gameOverSelected == DEAD_GO_END) {
		restart = true;
	}

	mc_init();
//	bg_prepBackground();
	bg_init();
	items_init();
	obstacle_init();
	level_restartLevel();

	skipDraw = true;

}

void dead_update(void) {

	++dead_timer;

	//this resets EVERYTHING
	if (dead_timer > DEAD_TIME) {
		//show game over screen
		dead_gameOverScreen();
		return;
		//start();
	}

	//calc positions
	//dead_halfPos += (DEAD_VELOCITY / 2);

	dead_minusX += DEAD_VELOCITY;
	dead_plusX -= DEAD_VELOCITY;

	dead_minusY -= DEAD_VELOCITY;
	dead_plusY += DEAD_VELOCITY;

}



#define RENDER_PART(x, y, tile) sprite_renderFromSheetAtSize(dead_##x, dead_##y, false, sheet, 10, 48)

void dead_render(pvr_ptr_t sheet) {


	temp2 = 0;

	RENDER_PART(sourceX, minusY, 3);
	RENDER_PART(plusX, minusY, 5);
	RENDER_PART(plusX, sourceY, 21);
	RENDER_PART(plusX, plusY, 37);
	RENDER_PART(sourceX, plusY, 35);
	RENDER_PART(minusX, plusY, 33);
	RENDER_PART(minusX, sourceY, 16);
	RENDER_PART(minusX, minusY, 2);

}

