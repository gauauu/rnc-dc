
#include "title.h"
#include "ctrl.h"
#include "sprite.h"
#include "mc.h"
#include "text.h"
#include "dead.h"

u16 title_timer;

u8 title_animFrame;
u8 title_animTimer;


char * credits[] = {
				"",
				"Game Design, Programming",
				"and Music",
				"by Nathan tolbert",
				"bitethechili.com",
				"",
				"BG Gfx - Omega Team by surt",
				"opengameart.org/content/omega-team",
				"CC-by 3.0 Modified for nes",
				"",
				"Robo-Ninja character art",
				"by Chris Hildenbrand",
				"",
				"",
				"https://opengameart.org/content/",
				"     2d-obstacle-collection",
				"https://creativecommons.org/licenses/by/4.0/",
				"Game Developer Studio",
				"Modified for game",
				"",
				"https://opengameart.org/content/",
				"     sci-fi-blue-pillar",
				"https://creativecommons.org/licenses/by/4.0/",
				"FunWithPixels",
				"Modified for game",
};


char * credits2[] = {
				"",
				"Item icons by Rob Eden",
				"",
				"Dj Rkod - Pulse (George Ellinas Remix) ",
				"CC-BY 3.0, George Ellinas",
				"http://ccmixter.org/files/George_Ellinas/14073",
				"",
				"Black Ops Requiem (Goa_Constrictor_Mix)",
				"CC-BY-SA 3.0, stellarartwars",
				"http://ccmixter.org/files/stellarartwars/41579",
				"",
				"Positive",
				"CC Sampling+, Mana Junkie",
				"http://ccmixter.org/files/mana_junkie/29319",
				"",
				"Alex Beroza - Art Now",
				"CC-BY 3.0, Alex Beroza",
				"http://dig.ccmixter.org/files/AlexBeroza/30344",
				"",
				"nihil-ace-spaceship-building-pack-expansion",
				"CC-BY 3.0, Buch",
				"https://opengameart.org/users/buch"

};

void title_allLoopStuff() {


	text_debugLine = 1;
	//do stuff to keep things acting nice
	ctrl_update();
}



void title_showRoboNinja(void) {
	int cel;

	if (title_animFrame < 4) {
		cel = title_animFrame;
	} else {
		cel = 7 - title_animFrame;
	}

	sprite_renderFromSheet(DCRES(120), DCRES(80) + 20, false, mc_title_sheet, cel);
}



void title_renderDifficultySelector(void) {
	cj = 22 * 8 - 2;
	if (mc_maxHealth == 2) {
		cj = 20 * 8 - 2;
	}
	ci = 11 * 8;
	sprite_renderFromSheetAtSize(DCRES(ci) - 16 + 48, DCRES(cj) - 28 + 48, false, mc_sheet, 11, 32);

}


void title_switchDifficulty(void) {
	mc_maxHealth = 2 - mc_maxHealth;
}


void title_run(void) {

	mc_deaths = 0;


	title_timer = 0;
	title_animFrame = 0;
	title_animTimer = TITLE_ANIM_TIME;

	while(true) {
		title_allLoopStuff();

		pvr_wait_ready();
		pvr_scene_begin();

		pvr_list_begin(PVR_LIST_OP_POLY);
		sprite_scaleToBackground(256, title_tex);
		pvr_list_finish();

		pvr_list_begin(PVR_LIST_TR_POLY);

		text_drawTextCenteredBig("ROBO-NINJA", 110);
		text_drawTextCenteredBig("CLIMB", 160);
		title_showRoboNinja();
		DRAW_TEXT_CENTERED("By Nathan Tolbert", 23);
		DRAW_TEXT_CENTERED("Press Start", 25);
		pvr_list_finish();
		pvr_scene_finish();



		++title_timer;

		--title_animTimer;
		if (title_animTimer == 0) {
			title_animTimer = TITLE_ANIM_TIME;
			++title_animFrame;
			if (title_animFrame == 6) {
				title_animFrame = 0;
			}
		}


		if (ctrl_new & PAD_A || title_timer > TITLE_CREDIT_TIME) {
			credits_run();
			title_timer = 0;
		}

		if (ctrl_current & PAD_START) {
			break;
		}

	}


	//////when start pressed, switch to the difficulty menu
	while(true) {
		title_allLoopStuff();

		pvr_wait_ready();
		pvr_scene_begin();

		pvr_list_begin(PVR_LIST_OP_POLY);
		sprite_scaleToBackground(256, title_tex);
		pvr_list_finish();

		pvr_list_begin(PVR_LIST_TR_POLY);
		title_renderDifficultySelector();
		title_showRoboNinja();
		DRAW_TEXT("Easy", 15, 23);
		DRAW_TEXT("Hard", 15, 25);
		pvr_list_finish();
		pvr_scene_finish();


		++title_timer;

		--title_animTimer;
		if (title_animTimer == 0) {
			title_animTimer = TITLE_ANIM_TIME;
			++title_animFrame;
			if (title_animFrame == 6) {
				title_animFrame = 0;
			}
		}


		if ((ctrl_new & PAD_A) || (ctrl_new & PAD_START)){
			break;
		}

		if ((ctrl_new & PAD_UP) ||
		    (ctrl_new & PAD_DOWN) ||
		    (ctrl_new & PAD_SELECT)) {
			title_switchDifficulty();
		}

	}



}


void credits_run(void) {

	int i;

	title_timer = 0;
	while(true) {
		title_allLoopStuff();

		pvr_wait_ready();
		pvr_scene_begin();

		pvr_list_begin(PVR_LIST_OP_POLY);
		sprite_scaleToBackground(256, title_tex);
		pvr_list_finish();

		pvr_list_begin(PVR_LIST_TR_POLY);


		//show credits
		for (i = 0; i < ARR_LEN(credits); i++) {
			text_drawTextSmall(credits[i], 20, 18 * i + 30);
		}


		pvr_list_finish();
		pvr_scene_finish();

		++title_timer;

		if (ctrl_new & PAD_A || title_timer > TITLE_CREDIT_TIME) {
			break;
		}


		if (ctrl_current & PAD_START) {
			break;
		}

	}

	//credits page 2
	title_timer = 0;
	while(true) {
		title_allLoopStuff();

		pvr_wait_ready();
		pvr_scene_begin();

		pvr_list_begin(PVR_LIST_OP_POLY);
		sprite_scaleToBackground(256, title_tex);
		pvr_list_finish();

		pvr_list_begin(PVR_LIST_TR_POLY);


		//show credits
		for (i = 0; i < ARR_LEN(credits2); i++) {
			text_drawTextSmall(credits2[i], 20, 18 * i + 30);
		}


		pvr_list_finish();
		pvr_scene_finish();

		++title_timer;

		if (ctrl_new & PAD_A || title_timer > TITLE_CREDIT_TIME) {
			break;
		}


		if (ctrl_current & PAD_START) {
			break;
		}

	}




}

void victory_drawRoboNinja(void) {
}

void victory_setup(void) {

}

void victory_run(void) {

	dead_loadDeathTextBuffer();


	victory_setup();

	title_timer = 0;
	while(true) {
		title_allLoopStuff();


		pvr_wait_ready();
		pvr_scene_begin();

		pvr_list_begin(PVR_LIST_OP_POLY);
		sprite_scaleToBackground(256, back_tex);
		pvr_list_finish();

		pvr_list_begin(PVR_LIST_TR_POLY);
		text_drawTextCentered("Congratulations!", 115);
		if (mc_maxHealth > 0) {
			text_drawTextSmall("Now try hard mode!", 200, 320);
		}

		text_drawTextSmall(dead_deathBuffer, 200, 340);


		title_showRoboNinja();

		pvr_list_finish();
		pvr_scene_finish();

		++title_timer;

		if (ctrl_new & PAD_START) {
			break;
		}

	}

	restart = true;

}