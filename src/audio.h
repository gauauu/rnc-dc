
#ifndef RNC_DC_AUDIO_H
#define RNC_DC_AUDIO_H

#include "common.h"

#define MUSIC_MAIN_THEME 0
#define MUSIC_CAVE 1
#define MUSIC_POSITIVE 2
#define MUSIC_ART 3


void audio_init();
void music_playSong(int songId);

void sfx_playPowerup();
void sfx_playZap();
void sfx_playExplode();

#endif //RNC_DC_AUDIO_H
