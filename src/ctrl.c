
#include "ctrl.h"
#include "text.h"

u8 ctrl_current;
u8 ctrl_new;



char ctrl_debugBuffer[20];
void ctrl_debug() {
	sprintf(ctrl_debugBuffer, "%02X / %02X", ctrl_current, ctrl_new);
	text_drawTextSmall(ctrl_debugBuffer, 40, 40);
}

void ctrl_update(void) {
	u8 ctrl_old = ctrl_current;
	ctrl_current = 0;

	MAPLE_FOREACH_BEGIN(MAPLE_FUNC_CONTROLLER, cont_state_t, st)

				if (st->buttons & CONT_START) {
					ctrl_current |= PAD_START;
				}

				if (st->joyx > 0) {
					ctrl_current |= PAD_RIGHT;
				}
				if (st->joyx < 0) {
					ctrl_current |= PAD_LEFT;
				}
				if (st->joyy < 0) {
					ctrl_current |= PAD_UP;
				}
				if (st->joyy > 0) {
					ctrl_current |= PAD_DOWN;
				}

				if (st->buttons & CONT_B) {
					ctrl_current |= PAD_A;
				}
				if (st->buttons & CONT_A) {
					ctrl_current |= PAD_A;
				}
				if (st->buttons & CONT_X) {
					ctrl_current |= PAD_B;
				}
				if (st->buttons & CONT_Y) {
					ctrl_current |= PAD_B;
				}

				if (st->buttons & CONT_DPAD_LEFT) {
					ctrl_current |= PAD_LEFT;
				}
				if (st->buttons & CONT_DPAD_RIGHT) {
					ctrl_current |= PAD_RIGHT;
				}
				if (st->buttons & CONT_DPAD_UP) {
					ctrl_current |= PAD_UP;
				}
				if (st->buttons & CONT_DPAD_DOWN) {
					ctrl_current |= PAD_DOWN;
				}

	MAPLE_FOREACH_END();


				ctrl_new = (ctrl_old ^ ctrl_current) & ctrl_current;


}
