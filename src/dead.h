
#ifndef RNC_DC_DEAD_H
#define RNC_DC_DEAD_H

#include "common.h"


void dead_begin(void);
void dead_update(void);
void dead_render(pvr_ptr_t sheet);



void dead_loadDeathTextBuffer(void);

extern char dead_deathBuffer[];


#endif //RNC_DC_DEAD_H
