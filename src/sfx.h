
#ifndef RNC_DC_SFX_H
#define RNC_DC_SFX_H

#define SFX_HIT       0
#define SFX_POWERUP   1

void sfx_init(void);

void sfx_playSound(int sfx);

#endif //RNC_DC_SFX_H
