
#ifndef RNC_DC_BG_H
#define RNC_DC_BG_H

#include "common.h"

extern u16 camera_y;
extern u16 camera_screen;
extern u16 camera_vy;

extern u8 bg_offscreenRow;
extern u16 bg_offscreenRowAddress;
extern u16 bg_offscreenAttrAddress;

extern u8 bg_tileModifier;


//render queue is now hi,lo,len,values
extern u8 bg_renderQueue[];
extern u8 bg_renderIndex;

extern u8 bg_attrRowToRender;
extern u8 bg_attrMirror[];


void bg_update(void);
void bg_renderOpaque(pvr_ptr_t tex, pvr_ptr_t tex2);
void bg_renderAlpha(pvr_ptr_t obs_sheet);
void bg_init(void);
void bg_prepBackground(void);
void bg_processRenderQueue(void);

//copies 8 attr bytes from
//the source map (temple_nt) to
//bg_attrMirror, starting at offset temp
void bg_loadAttrMirrorRow(void);


#endif //RNC_DC_BG_H
