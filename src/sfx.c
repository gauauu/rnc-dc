#include "sfx.h"
#include "audio.h"

void sfx_playSound(int sfxId) {
	switch (sfxId) {
		case SFX_POWERUP:
			sfx_playPowerup();
			break;
		case SFX_HIT:
			sfx_playExplode();
			break;
	}

}


