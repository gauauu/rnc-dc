//
// Created by tolbert on 4/16/19.
//

#include "sprite.h"
#include <png/png.h>



pvr_ptr_t sprite_loadTexture(char * name, int w, int h) {
	pvr_ptr_t tex_ptr;
	tex_ptr = pvr_mem_malloc(w * h * 4);
	png_to_texture(name, tex_ptr, PNG_MASK_ALPHA);
	return tex_ptr;
}


void sprite_renderFromSheetAtSize(int screen_x, int screen_y, bool hFlip, pvr_ptr_t texture, int cel, int size) {
	pvr_poly_cxt_t cxt;
	pvr_poly_hdr_t hdr;
	pvr_vertex_t vert;

	int sheetRow;
	int sheetCol;

	float l = 0.0;
	float r = 0.0;
	float t = 0.0;
	float b = 0.0;

	float swapper;

	sheetRow = cel / 4;
	sheetCol = cel % 4;

	l = 0.25f * sheetCol;
	r = l + 0.25f;

	t = 0.25f * sheetRow;
	b = t + 0.25f;

	if (hFlip) {
		swapper = l;
		l = r;
		r = swapper;
	}


	pvr_poly_cxt_txr(&cxt, PVR_LIST_TR_POLY, PVR_TXRFMT_ARGB1555, 512, 512, texture, PVR_FILTER_BILINEAR);
	pvr_poly_compile(&hdr, &cxt);
	pvr_prim(&hdr, sizeof(hdr));

	vert.argb = PVR_PACK_COLOR(1.0f, 1.0f, 1.0f, 1.0f);
	vert.oargb = 0;
	vert.flags = PVR_CMD_VERTEX;

	vert.x = screen_x;
	vert.y = screen_y;
	vert.z = 1;
	vert.u = l;
	vert.v = t;
	pvr_prim(&vert, sizeof(vert));

	vert.x = screen_x + size;
	vert.y = screen_y;
	vert.z = 1;
	vert.u = r;
	vert.v = t;
	pvr_prim(&vert, sizeof(vert));

	vert.x = screen_x;
	vert.y = screen_y + size;
	vert.z = 1;
	vert.u = l;
	vert.v = b;
	pvr_prim(&vert, sizeof(vert));

	vert.x = screen_x + size;
	vert.y = screen_y + size;
	vert.z = 1;
	vert.u = r;
	vert.v = b;
	vert.flags = PVR_CMD_VERTEX_EOL;
	pvr_prim(&vert, sizeof(vert));
}


void sprite_renderFromSheet(int screen_x, int screen_y, bool hFlip, pvr_ptr_t texture, int cel) {
	sprite_renderFromSheetAtSize(screen_x, screen_y, hFlip, texture, cel, 128);
}


void sprite_renderElec(int screen_x, int screen_y, int w, int xStart, pvr_ptr_t texture) {
	pvr_poly_cxt_t cxt;
	pvr_poly_hdr_t hdr;
	pvr_vertex_t vert;


	float t = 0.75;
	float b = 1.0;

	float startPct = (float)xStart /  (float)512;
	float wPct = (float)w /  (float)512;

	pvr_poly_cxt_txr(&cxt, PVR_LIST_TR_POLY, PVR_TXRFMT_ARGB1555, 512, 512, texture, PVR_FILTER_BILINEAR);
	pvr_poly_compile(&hdr, &cxt);
	pvr_prim(&hdr, sizeof(hdr));

	vert.argb = PVR_PACK_COLOR(1.0f, 1.0f, 1.0f, 1.0f);
	vert.oargb = 0;
	vert.flags = PVR_CMD_VERTEX;

	vert.x = screen_x;
	vert.y = screen_y;
	vert.z = 1;
	vert.u = startPct;
	vert.v = t;
	pvr_prim(&vert, sizeof(vert));

	vert.x = screen_x + w;
	vert.y = screen_y;
	vert.z = 1;
	vert.u = startPct + wPct;
	vert.v = t;
	pvr_prim(&vert, sizeof(vert));

	vert.x = screen_x;
	vert.y = screen_y + 32;
	vert.z = 1;
	vert.u = startPct;
	vert.v = b;
	pvr_prim(&vert, sizeof(vert));

	vert.x = screen_x + w;
	vert.y = screen_y + 32;
	vert.z = 1;
	vert.u = startPct + wPct;
	vert.v = b;
	vert.flags = PVR_CMD_VERTEX_EOL;
	pvr_prim(&vert, sizeof(vert));
}

void sprite_render(int screen_x, int screen_y, int w, int h, bool hFlip, pvr_ptr_t texture) {
	pvr_poly_cxt_t cxt;
	pvr_poly_hdr_t hdr;
	pvr_vertex_t vert;

	float l = 0.0;
	float r = 1.0;

	if (hFlip) {
		l = 1.0;
		r = 0.0;
	}

	pvr_poly_cxt_txr(&cxt, PVR_LIST_TR_POLY, PVR_TXRFMT_ARGB1555, w, h, texture, PVR_FILTER_BILINEAR);
	pvr_poly_compile(&hdr, &cxt);
	pvr_prim(&hdr, sizeof(hdr));

	vert.argb = PVR_PACK_COLOR(1.0f, 1.0f, 1.0f, 1.0f);
	vert.oargb = 0;
	vert.flags = PVR_CMD_VERTEX;

	vert.x = screen_x;
	vert.y = screen_y;
	vert.z = 1;
	vert.u = l;
	vert.v = 0.0;
	pvr_prim(&vert, sizeof(vert));

	vert.x = screen_x + w;
	vert.y = screen_y;
	vert.z = 1;
	vert.u = r;
	vert.v = 0.0;
	pvr_prim(&vert, sizeof(vert));

	vert.x = screen_x;
	vert.y = screen_y + h;
	vert.z = 1;
	vert.u = l;
	vert.v = 1.0;
	pvr_prim(&vert, sizeof(vert));

	vert.x = screen_x + w;
	vert.y = screen_y + h;
	vert.z = 1;
	vert.u = r;
	vert.v = 1.0;
	vert.flags = PVR_CMD_VERTEX_EOL;
	pvr_prim(&vert, sizeof(vert));
}

void sprite_scaleToBackground(int textureSize, pvr_ptr_t tex) {
	pvr_poly_cxt_t cxt;
	pvr_poly_hdr_t hdr;
	pvr_vertex_t vert;

	pvr_poly_cxt_txr(&cxt, PVR_LIST_OP_POLY, PVR_TXRFMT_RGB565, textureSize, textureSize, tex, PVR_FILTER_BILINEAR);
	pvr_poly_compile(&hdr, &cxt);
	pvr_prim(&hdr, sizeof(hdr));

	vert.argb = PVR_PACK_COLOR(1.0f, 1.0f, 1.0f, 1.0f);
	vert.oargb = 0;
	vert.flags = PVR_CMD_VERTEX;

	vert.x = 0;
	vert.y = 0;
	vert.z = 1;
	vert.u = 0.0;
	vert.v = 0.0;
	pvr_prim(&vert, sizeof(vert));

	vert.x = 640;
	vert.y = 0;
	vert.z = 1;
	vert.u = 1.0;
	vert.v = 0.0;
	pvr_prim(&vert, sizeof(vert));

	vert.x = 0;
	vert.y = 480;
	vert.z = 1;
	vert.u = 0.0;
	vert.v = 1.0;
	pvr_prim(&vert, sizeof(vert));

	vert.x = 640;
	vert.y = 480;
	vert.z = 1;
	vert.u = 1.0;
	vert.v = 1.0;
	vert.flags = PVR_CMD_VERTEX_EOL;
	pvr_prim(&vert, sizeof(vert));

}

void sprite_renderBackdrop(int offset, pvr_ptr_t tex) {
		pvr_poly_cxt_t cxt;
		pvr_poly_hdr_t hdr;
		pvr_vertex_t vert;

		pvr_poly_cxt_txr(&cxt, PVR_LIST_OP_POLY, PVR_TXRFMT_RGB565, 256, 256, tex, PVR_FILTER_BILINEAR);
		pvr_poly_compile(&hdr, &cxt);
		pvr_prim(&hdr, sizeof(hdr));

		vert.argb = PVR_PACK_COLOR(1.0f, 1.0f, 1.0f, 1.0f);
		vert.oargb = 0;
		vert.flags = PVR_CMD_VERTEX;

		vert.x = 0;
		vert.y = offset;
		vert.z = 1;
		vert.u = 0.0;
		vert.v = 0.0;
		pvr_prim(&vert, sizeof(vert));

		vert.x = 640;
		vert.y = offset;
		vert.z = 1;
		vert.u = 1.0;
		vert.v = 0.0;
		pvr_prim(&vert, sizeof(vert));

		vert.x = 0;
		vert.y = offset + 480;
		vert.z = 1;
		vert.u = 0.0;
		vert.v = 1.0;
		pvr_prim(&vert, sizeof(vert));

		vert.x = 640;
		vert.y = offset + 480;
		vert.z = 1;
		vert.u = 1.0;
		vert.v = 1.0;
		vert.flags = PVR_CMD_VERTEX_EOL;
		pvr_prim(&vert, sizeof(vert));
}