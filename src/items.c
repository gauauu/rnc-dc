
#include "items.h"
#include "level.h"
#include "bg.h"
#include "mc.h"
#include "sfx.h"
#include "sprite.h"
#include "audio.h"


#define NUM_ITEMS 4

#define ITEM_TILE_DOUBLE_JUMP 4
#define ITEM_TILE_PAUSE_JUMP 5
#define ITEM_TILE_ROCKET 6

u8 items_owned[NUM_ITEMS];
u8 items_x;
u8 items_y;
u8 items_current;
u8 items_screen;

u8 items_cycled;

void items_init(void){
	for (cj = 0; cj < NUM_ITEMS; ++cj) {
		//auto-add double jump
		if (cj == ITEM_DOUBLE_JUMP && level_current > 2) {
			items_owned[cj] = 1;
			continue;
		}
		//auto-add pause jump
		if (cj == ITEM_PAUSE_JUMP && level_current > 3) {
			items_owned[cj] = 1;
			continue;
		}
		items_owned[cj] = 0;
	}
	items_current = 0;
	items_x = 0;
	items_y = 0;
	items_screen = camera_screen;

}

void items_spawnForLevel(void) {
	items_y = MSB(camera_y) - 16;
	items_screen = camera_screen;
	items_x = 100;

	if (level_current == 2) {
		items_current = ITEM_DOUBLE_JUMP;
	}
	if (level_current == 3) {
		items_current = ITEM_PAUSE_JUMP;
	}
	if (level_current == 4) {
		items_current = ITEM_ROCKET;
	}
	items_cycled = false;
}

//temp is item number
//ci is x
//cj is y
void items_spawn(void){
	items_x = ci;
	items_y = ci;
	items_current = temp;
	items_cycled = false;
}

void items_update(void){
	//if offscreen, kill
	//(this is done in the render function
	//because we already know the relative position there)

	if (items_current == 0) {
		return;
	}

	//check collision against mc, acquire if necessary
	tempU16A = MSB(mc_x);
	tempU16A += MC_COL_WIDTH;

	tempU16B = MSB(mc_y);
	tempU16B += MC_COL_HEIGHT;


	tempU16C = items_x;
	tempU16C += 16;
	tempU16D = items_y;
	tempU16D += 16;

	if (MSB(mc_y) > tempU16D) {
		return;
	}
	if (tempU16B < items_y) {
		return;
	}

	if (MSB(mc_x) > tempU16C) {
		return;
	}
	if (tempU16A < items_x) {
		return;
	}

	//acquire item!
	sfx_playPowerup();
	items_owned[items_current] = 1;
	items_current = 0;



}

#define ITEM_ICON_SPACING 24

void items_renderOwned(void) {
	if(items_owned[ITEM_DOUBLE_JUMP]) {
		temp = ITEM_TILE_DOUBLE_JUMP;
		temp2 = 0;
		ci = 16;
		cj = 16;
		tempA = 2;
		tempB = 2;
		sprite_renderFromSheetAtSize(DCRES(ci), DCRES(cj), false, mc_title_sheet, temp, 32);
	}
	if(items_owned[ITEM_PAUSE_JUMP]) {
		temp = ITEM_TILE_PAUSE_JUMP;
		temp2 = 0;
		ci = 16;
		cj = 16 + ITEM_ICON_SPACING;
		tempA = 2;
		tempB = 2;
		sprite_renderFromSheetAtSize(DCRES(ci), DCRES(cj), false, mc_title_sheet, temp, 32);
	}

	if(items_owned[ITEM_ROCKET]) {
		temp = ITEM_TILE_ROCKET;
		temp2 = 0;
		ci = 16;
		cj = 16 + (ITEM_ICON_SPACING * 2);
		tempA = 2;
		tempB = 2;
		sprite_renderFromSheetAtSize(DCRES(ci), DCRES(cj), false, mc_title_sheet, temp, 32);
	}
}

void items_render(void){

	items_renderOwned();

	ci = items_x;
	cj = items_y;
	if (ci == 0 || items_current == 0) {
		return;
	}


	temp = (240 - MSB(camera_y));
	if (camera_screen != items_screen) {
		temp -= 16;
	}

	cj += temp;
	if (cj > 220 && cj < 240) {
		if (items_cycled) {
			//time to kill it.
			items_current = 0;
		}
		return;
	}

	items_cycled = true;


	switch (items_current){
		case ITEM_DOUBLE_JUMP:
			temp = ITEM_TILE_DOUBLE_JUMP;
			break;
		case ITEM_PAUSE_JUMP:
			temp = ITEM_TILE_PAUSE_JUMP;
			break;
		case ITEM_ROCKET:
			temp = ITEM_TILE_ROCKET;
			break;
	}

	tempA = 2;
	tempB = 2;
	temp2 = 0;
//	sprite_drawBlock();


	sprite_renderFromSheetAtSize(DCXOFFSET(DCRES(ci) + 20), DCRES(cj), false, mc_title_sheet, temp, 32);

}



