#ifndef RNC_DC_COMMON_H
#define RNC_DC_COMMON_H

#ifndef _arch_dreamcast
#define _arch_dreamcast
#endif

#include <kos.h>
#include <string.h>
#include "binary.h"

#define u8 unsigned char
#define u16 unsigned short
#define s8 signed char
#define s16 signed short

#define MSB(x)			((u8)(((u16)x)>>8))
#define LSB(x)			((u8)(((u16)x)&0xff))


#define bool u8
#define true 1
#define false 0


#define PAD_A       128
#define PAD_B       64
#define PAD_SELECT  32
#define PAD_START   16
#define PAD_UP      8
#define PAD_DOWN    4
#define PAD_LEFT    2
#define PAD_RIGHT   1

#define FACING_RIGHT 0
#define FACING_LEFT  1

#define FLIP_HORIZ B01000000


// macros to translate from NES-style screen coordinates to Dreamcast (approx double resolution)
#define SCREEN(val) ((((u16)MSB(val)) * 2) + (LSB(val) >> 7))
#define DCRES(val) (((s16)(val)) * 2)
#define DCXOFFSET(val) (((u16)(val)) + 30 + 15)

extern u8 ci;
extern u8 cj;
extern u8 temp;
extern u8 temp2;
extern u8 temp3;
extern s8 tempS8;
extern u8 * tempPtr;
extern u8 * tempPtr2;
extern u8 VBLANK_FLAG;
extern u8 timer;
extern u8 * tempPtrA;
extern u8 * tempPtrB;
extern u8 * tempPtrC;
extern u8 * tempPtrD;

extern u16 tempU16;
extern s16 tempS16;

extern u16 tempU16A;
extern u16 tempU16B;
extern u16 tempU16C;
extern u16 tempU16D;

extern u8 tempA;
extern u8 tempB;
extern u8 tempC;
extern u8 tempD;
extern u8 tempE;
extern u8 tempF;
extern u8 tempG;
extern u8 tempH;
extern u8 tempj;
extern u8 tempJ;
extern u8 tempK;

extern char debugBuffer[];

extern pvr_ptr_t font_tex;
extern pvr_ptr_t back_tex;
extern pvr_ptr_t back_tex2;

extern pvr_ptr_t load_bg_tex;
extern pvr_ptr_t title_tex;

pvr_ptr_t mc_sheet;
pvr_ptr_t mc_title_sheet;
pvr_ptr_t obstacle_sheet;

#define ARR_LEN(x) (sizeof(x) / sizeof((x)[0]))

extern bool skipDraw;
extern bool restart;

#endif //RNC_DC_COMMON_H
