#!/usr/bin/python

import xml.etree.ElementTree as etree    
import sys
import os
import re
import ntpath

def level_to_data(input_file):
    file_name = ntpath.basename(input_file)
    level_id = os.path.splitext(file_name)[0]

    input_file = open(input_file, "r")
    output_file = open("bin/gen/" + level_id + ".c", "w")

    to_zero = re.compile("/[.]/")
    to_one = re.compile("/[^.]/")

    output_file.write("#include \"common.h\"\n")

    output_file.write("const u8 " + level_id + "[] = {\n")

    i = -1;
    for line in input_file:
        line = line.rstrip()
        line = re.sub("[.]", "0", line)
        line = re.sub("[^0]", "1", line)
        line = "  B" + line + ",\n"
        output_file.write(line)
        i = i + 1

    output_file.write("};\n")

    output_file.write("const u8 "  + level_id + "_max[] = { " + str(i) + "};\n")

    input_file.close()
    output_file.close()




if (len(sys.argv) <= 1):
        print("Usage level2Data.py [level_file]\n")
        exit(1)

if __name__ == "__main__":
    level_file = sys.argv[1]
    level_to_data(level_file)
