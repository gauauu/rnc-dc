#!/usr/bin/python

import xml.etree.ElementTree as etree    
import sys
import os
import re
import ntpath
import glob

def generate_level_pointers():

    levels = glob.glob('levels/*.lvl')
    levels.sort()

    mapsPointers = ""
    maxPointers = ""

    text1Pointers = ""
    text2Pointers = ""

    inst0Pointers = ""
    inst1Pointers = ""
    externs = ""

    for level_file in levels:
        file_name = ntpath.basename(level_file)
        level_id = os.path.splitext(file_name)[0]

        mapsPointers += " (u8 *) &" + level_id + ",\n"

        maxPointers += " (u8 *) &" + level_id + "_max,\n"

        text1Pointers += "  (u8 *)&text_" + level_id + ",\n"

        text2Pointers += " (u8 *) &text_" + level_id + "title,\n"

        inst0Pointers += " (u8 *) &text_" + level_id + "inst0,\n"
        inst1Pointers += " (u8 *) &text_" + level_id + "inst1,\n"


        externs += "extern const u8 " + level_id + ";\n"
        externs += "extern const u8 " + level_id + "_max;\n"
        externs += "extern const u8 text_" + level_id + ";\n"
        externs += "extern const u8 text_" + level_id + "title;\n"

        externs += "extern const u8 text_" + level_id + "inst0;\n"
        externs += "extern const u8 text_" + level_id + "inst1;\n"

    

    output_file = open("bin/gen/levelPointers.c", "w")

    output_file.write("#include \"common.h\"\n")
    output_file.write(externs)


    output_file.write("const u8* level_maps[] = {\n")
    output_file.write(mapsPointers)
    output_file.write("};\n")

    output_file.write("const u8* level_maxes[] = {\n")
    output_file.write(maxPointers)
    output_file.write("};\n")

    output_file.write("const u8* level_text1Pointers[] = {\n")
    output_file.write(text1Pointers)
    output_file.write("};\n")

    output_file.write("const u8* level_text2Pointers[] = {\n")
    output_file.write(text2Pointers)
    output_file.write("};\n")

    output_file.write("const u8* level_inst0Pointers[] = {\n")
    output_file.write(inst0Pointers)
    output_file.write("};\n")

    output_file.write("const u8* level_inst1Pointers[] = {\n")
    output_file.write(inst1Pointers)
    output_file.write("};\n")


    output_file.close()


if __name__ == "__main__":
    generate_level_pointers()
    
