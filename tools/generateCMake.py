#!/usr/bin/python

import os
from os import listdir
from os.path import isfile, join


out = open("CMakeLists.txt", "w")
out.write("cmake_minimum_required(VERSION 3.7)\n")
out.write("set(CMAKE_CXX_STANDARD 11)\n")

files = "Makefile Makefile.kos"
onlyfiles = [f for f in listdir("src") if isfile(join("src", f))]

for filename in onlyfiles:
    files += " src/" + filename


out.write("set(SOURCE_FILES\n     " + files + ")\n")

out.write("add_executable(c ${SOURCE_FILES})\n")

out.close()


