# rnc-dc

Robo-Ninja Climb DC
===================

A Dreamcast port of my NES and Atari 2600 game, Robo-Ninja Climb.


The code was ported directly from NES C source, and so uses a lot of idioms necessary for performance on the 6502
that aren't recommended on modern platforms.
(Global shared variables, calling functions without parameters, using unsigned char as
much as possible, etc).  Please do not think that most of this is good example code for a general C codebase.


Playing
-------
You should be able to play this on hardware by burning it to a CD using ImgBurn with the Padus CDI plugin that you
can find on the [ImgBurn download page](http://www.imgburn.com/index.php?act=download). It may be safest to install burn using something like [Ninite](https://ninite.com/) to avoid
any adware that they bundle with it.

Or you can play on an emulator such as [ReDream](https://redream.io/)

License
--------
All code can be used under the
https://creativecommons.org/licenses/by-nc-nd/3.0/
(You can view and share it non-commercially, but no derivative works)
If you'd like to use this under a different license, feel free to contact me.

Art and music are the property of their respective owners (see credits below)




>				Game Design, Programming
>				and Music
>				by Nathan tolbert
>				bitethechili.com
>				Robo-Ninja character art
>				by Chris Hildenbrand
>
>				BG Gfx - Omega Team by surt
>				opengameart.org/content/omega-team
>				CC-by 3.0 Modified for nes
>
>
>				https://opengameart.org/content/2d-obstacle-collection
>				https://creativecommons.org/licenses/by/4.0/
>				Game Developer Studio
>				Modified for game
>
>				https://opengameart.org/content/sci-fi-blue-pillar
>				https://creativecommons.org/licenses/by/4.0/
>				FunWithPixels
>				Modified for game
>
>        Item icons by Rob Eden
>
>				Dj Rkod - Pulse (George Ellinas Remix)
>				CC-BY 3.0, George Ellinas
>				http://ccmixter.org/files/George_Ellinas/14073
>
>				Black Ops Requiem (Goa_Constrictor_Mix)
>				CC-BY-SA 3.0, stellarartwars
>				http://ccmixter.org/files/stellarartwars/41579
>
>				Positive
>				CC Sampling+, Mana Junkie
>				http://ccmixter.org/files/mana_junkie/29319
>
>				Alex Beroza - Art Now
>				CC-BY 3.0, Alex Beroza
>				http://dig.ccmixter.org/files/AlexBeroza/30344
>
>				nihil-ace-spaceship-building-pack-expansion
>				CC-BY 3.0, Buch
>				https://opengameart.org/users/buch

